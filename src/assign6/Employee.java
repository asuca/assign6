package assign6;

public class Employee {
	protected String name;
	protected String ssn;
	
	Employee(String name,String ssn) {
		this.name = name;
		this.ssn  = ssn;
	}
	
	public double salary() {
		return 0d;
	}
	
	public String toString() {
		return "Employee " + name + "\'s ssn is " + ssn;
	}
}
