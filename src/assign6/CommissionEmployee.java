package assign6;

public class CommissionEmployee extends Employee {
	protected double sales;
	protected double commission;
	public CommissionEmployee(String name, String ssn,double sales,double commission) {
		super(name,ssn);
		this.sales      = sales;
		this.commission = commission;
	}
	public double salary() {
		return sales * commission;
	}
	public String toString() {
		return super.toString() + " , and salary is " + salary();
	}
}
