package assign6;

public class HourlyEmployee extends Employee{
	protected double wage;
	protected double hours;
	public HourlyEmployee(String name, String ssn, double wage,double hours) {
		super(name,ssn);
		this.wage  = wage;
		this.hours = hours;
	}
	public double salary() {
		return wage * hours;
	}
	public String toString() {
		return super.toString() + ", and salary is " + salary();
	}
}
