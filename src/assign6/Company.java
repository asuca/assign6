package assign6;

public class Company {
	public static void main(String[] args) {
		Employee ee = new Employee("Jim","85471943");
		CommissionEmployee commEE = new 
				CommissionEmployee("David","79131955",40d,40d);
		HourlyEmployee HrEE = new HourlyEmployee("Robort","43579910",45d,160d);
		SalariedEmployee SalEE = new SalariedEmployee("Rose","96821570",9700);
		Employee[] employee = {ee,commEE,HrEE,SalEE}; 
		Payroll payroll = new Payroll(employee);
		payroll.paySalary();
	}
}
