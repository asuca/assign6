package assign6;

public class SalariedEmployee extends Employee{
	protected double basicSalary;
	
	public SalariedEmployee(String name,String ssn,double basicSalary) {
		super(name,ssn);
		this.basicSalary = basicSalary;
	}
	
	public double salary() {
		return basicSalary;
	}
	public String toString() {
		return super.toString() + ", and salary is " + salary();
	}
}
